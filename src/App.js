// import logo from './logo.svg';
import Header from "components/Header";
import ProjectView from "components/ProjectView";
import React, { Component } from "react";
import "./App.css";

class Home extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <ProjectView />
      </div>
    );
  }
}

export default Home;
