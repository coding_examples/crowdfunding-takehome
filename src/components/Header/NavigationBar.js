import { Form, Button } from "antd";
import React, { Component } from "react";
import { Navbar, Nav, NavDropdown, FormControl } from "react-bootstrap";
import styles from "./NavigationBar.module.css"
export default class NavigationBar extends Component {
  render() {
    return (
      <>
        <Navbar expand="lg" className={styles.navbar}>
          <Navbar.Brand href="#" className={styles.navbar__brand}>
            crowdfund
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav" className={styles.navbar__collapse}>
            <Nav className="ml-auto">
              <Nav.Link href="#">About</Nav.Link>
              <Nav.Link href="#">Discover</Nav.Link>
              <Nav.Link href="#">Get Started</Nav.Link>
            </Nav>
            {/* <Form inline>
              <FormControl
                type="text"
                placeholder="Search"
                className="mr-sm-2"
              />
              <Button variant="outline-success">Search</Button>
            </Form> */}
          </Navbar.Collapse>
        </Navbar>
      </>
    );
  }
}
