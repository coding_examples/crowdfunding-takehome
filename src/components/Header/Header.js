import React, { Component } from 'react'
import heroImage from "../../images/image-hero-desktop.jpg";
import NavigationBar from './NavigationBar';

export default class Header extends Component {
    render() {
        return (
          <div>
            <>
              <NavigationBar />
              <div>
                <img alt="Hero" src={heroImage} style={{width: "100%"}}/>
              </div>
            </>
          </div>
        );
    }
}
