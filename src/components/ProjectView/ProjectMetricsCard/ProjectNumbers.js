import React, { Component } from 'react'

export default class ProjectNumbers extends Component {
  render() {
    const {
      currency,
      totalBackedAmount,
      targetAmount,
      numBackers,
      numDaysLeft,
    } = this.props.data;
    return (
      <div>
        <div>
          <div>{`${currency}${totalBackedAmount}`}</div>
          <div>of {`${currency}${targetAmount}`} backed</div>
        </div>
        <div>
          <div>{`${numBackers}`}</div>
          <div>total backers</div>
        </div>
        <div>
          <div>{`${numDaysLeft}`}</div>
          <div>days left</div>
        </div>
      </div>
    );
  }
}