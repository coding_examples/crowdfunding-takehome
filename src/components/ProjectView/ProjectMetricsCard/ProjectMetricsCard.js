import { Progress } from 'antd';
import React, { Component } from 'react'
import ProjectNumbers from './ProjectNumbers';
import styles from "./ProjectMetricsCard.module.css"
export default 
class ProjectMetricsCard extends Component {
  render() {
    const {
      currency,
      numBackers,
      numDaysLeft,
      totalBackedAmount,
      targetAmount,
    } = this.props.data;
    const percentBacked = (totalBackedAmount / targetAmount) * 100;
    return (
      <div className={styles.container}>
        <div className={styles.metrics}>
          <div className={`${styles.metrics_column}`}>
            <h2>{`${currency}${totalBackedAmount}`}</h2>
            <p>of {`${currency}${targetAmount}`} backed</p>
          </div>
          <div className={`${styles.metrics_column} ${styles.vertical_line}`}>
            <h2>{`${numBackers}`}</h2>
            <p>total backers</p>
          </div>
          <div className={`${styles.metrics_column} ${styles.vertical_line}`}>
            <h2>{`${numDaysLeft}`}</h2>
            <p>days left</p>
          </div>
        </div>{" "}
        <Progress percent={percentBacked} showInfo={false} />
      </div>
    );
  }
}
