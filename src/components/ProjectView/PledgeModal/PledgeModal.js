import { Modal } from "antd";
import React, { Component } from "react";
import PledgeCard from "../PledgeCard";

export default class PledgeModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedPledgeId: "0",
      currentPledgeAmount: 0,
    };
  }

  cancelButtonHandler = () => {
    this.props.closeModalHandler();
  };

  pledgeCardChangeHandler = (id) => {
    if (id !== this.state.selectedPledgeId) {
      this.setState({
        currentPledgeAmount: 0,
      });
    }
    this.setState({
      selectedPledgeId: id,
    });
  };

  handlePledgeAmountChange = (amount) => {
    this.setState({
      currentPledgeAmount: amount
    });
  }

  resetStates = () => {
    this.setState({
      selectedPledgeId: "0",
      currentPledgeAmount: 0,
    });
  }
  submitPledgeHandler = (pledgeAmount) => {
    this.resetStates();
    this.props.submitPledgeHandler(this.state.selectedPledgeId, pledgeAmount);
  };

  render() {
    const { pledges, title, currency } = this.props.data;
    return (
      <div>
        <Modal
          visible={this.props.visible}
          // onOk={this.handleOk}
          onCancel={this.cancelButtonHandler}
          okButtonProps={{ disabled: true }}
          cancelButtonProps={{ disabled: true }}
          footer={null}
        >
          <h2>Back this project</h2>
          <div>Want to support us in bringing {title} out in the world?</div>
          <div>
            <PledgeCard
              currency={currency}
              data={{
                id: "0",
                title: "Pledge with no reward",
                description:
                  "Choose to support us without a reward if you simply believe in our project. As a backer, you will be signed up to receive product updates via email.",
              }}
              isPledgeModal={true}
              currentPledgeAmount={this.state.currentPledgeAmount}
              selectedPledgeId={this.state.selectedPledgeId}
              onPledgeCardClick={this.pledgeCardChangeHandler}
              onPledgeAmountChange={this.handlePledgeAmountChange}
              onContinueButtonClick={this.submitPledgeHandler}
            />
            {pledges.map((pledge, idx) => (
              <PledgeCard
                currency={currency}
                data={pledge}
                isPledgeModal={true}
                currentPledgeAmount={this.state.currentPledgeAmount}
                selectedPledgeId={this.state.selectedPledgeId}
                onPledgeCardClick={this.pledgeCardChangeHandler}
                onPledgeAmountChange={this.handlePledgeAmountChange}
                onContinueButtonClick={this.submitPledgeHandler}
              />
            ))}
          </div>
        </Modal>
      </div>
    );
  }
}
