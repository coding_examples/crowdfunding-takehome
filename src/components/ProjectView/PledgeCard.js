import { Button, message } from "antd";
import React, { Component } from "react";

export default class PledgeCard extends Component {
  onSelectRewardButtonClick = () => {
    this.props.onSelectRewardButtonClick();
  };

  onPledgeCardClick = (event) => {
    this.props.onPledgeCardClick(this.props.data.id);
  };

  onPledgeAmountChange = (event) => {
    let pledgeAmount = parseInt(event.target.value);
    this.props.onPledgeAmountChange(pledgeAmount);
  };

  isValidPledgeAmount = (amount) => {
    return (
      amount >= 0 &&
      (this.props.data.minPledgeAmount === undefined ||
        amount >= this.props.data.minPledgeAmount)
    );
  };

  onContinueButtonClick = (event) => {
    let pledgeAmount = this.props.currentPledgeAmount;
    debugger;

    if (this.isValidPledgeAmount(pledgeAmount)) {
      this.props.onContinueButtonClick(pledgeAmount);
    } else {
      message.error(
        `Choose a minimum pledge amount of ${this.props.data.minPledgeAmount || 0}`
      );
    }
  };

  render() {
    const { id, title, minPledgeAmount, description, numPledgesLeft } =
      this.props.data;
    const currency = this.props.currency;
    const isSelected = this.props.selectedPledgeId === id ? true : false;

    if (this.props.isPledgeModal) {
      return (
        <div>
          <label>
            <input
              type="radio"
              value={id}
              name="pledgeModal"
              checked={isSelected}
              onChange={this.onPledgeCardClick}
            />
            <div>
              <div>{title}</div>
              {minPledgeAmount !== undefined && (
                <>
                  <div>Pledge {`${currency}${minPledgeAmount}`} or more</div>
                  <div>
                    <span>{numPledgesLeft}left</span>
                  </div>
                </>
              )}
            </div>
            <p>{description}</p>
            {isSelected && (
              <div>
                <div>Enter your pledge</div>
                <div>
                  <input
                    type="number"
                    min={minPledgeAmount}
                    value={this.props.currentPledgeAmount}
                    onChange={this.onPledgeAmountChange}
                  />
                  <button onClick={this.onContinueButtonClick}>Continue</button>
                </div>
              </div>
            )}
          </label>
        </div>
      );
    } else {
      return (
        <div>
          <div>
            <div>{title}</div>
            <div>Pledge {`${currency}${minPledgeAmount}`} or more</div>
          </div>
          <div>{description}</div>
          <div>
            <span>{numPledgesLeft}left</span>
          </div>
          <button onClick={this.onSelectRewardButtonClick}>
            Select Reward
          </button>
        </div>
      );
    }
  }
}
