import React, { Component } from "react";
import PledgeModal from "./PledgeModal";
import ProjectDetailsCard from "./ProjectDetailsCard";
import ProjectInfoCard from "./ProjectInfoCard";
import ProjectMetricsCard from "./ProjectMetricsCard";
import styles from "./ProjectView.module.css";

const DATA = {
  title: "Mastercraft Bamboo Monitor Riser",
  tagline:
    "A beautiful & handcrafted monitor stand to reduce neck and eye strain.",
  description: [
    "The Mastercraft Bamboo Monitor Riser is a sturdy and stylish platform that elevates your screen to a more comfortable viewing height. Placing your monitor at eye level has the potential to improve your posture and make you more comfortable while at work, helping you stay focused on the task at hand.",
    "Featuring artisan craftsmanship, the simplicity of design creates extra desk space below your computer to allow notepads, pens, and USB sticks to be stored under the stand.",
  ],
  currency: "$",
  totalBackedAmount: 89914,
  targetAmount: 100000,
  numBackers: 5007,
  numDaysLeft: 56,
  pledges: [
    {
      id: "1",
      title: "Bamboo Stand",
      minPledgeAmount: 25,
      description:
        "You get an ergonomic stand made of natural bamboo. You've helped us launch our promotional campaign, and you’ll be added to a special Backer member list.",
      numPledgesLeft: 101,
    },
    {
      id: "2",
      title: "Black Edition Stand",
      minPledgeAmount: 75,
      description:
        "You get a Black Special Edition computer stand and a personal thank you. You’ll be added to our Backer member list. Shipping is included.",
      numPledgesLeft: 64,
    },
    {
      id: "3",
      title: "Mahogany Special Edition",
      minPledgeAmount: 200,
      description:
        "You get two Special Edition Mahogany stands, a Backer T-Shirt, and a personal thank you. You’ll be added to our Backer member list. Shipping is included.",
      numPledgesLeft: 0,
    },
  ],
};
export default class ProjectView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isBookmarked: false,
      userPledgeTransactions: [
        { pledgeId: "2", pledgeAmount: 100, pledgeTime: new Date() },
      ],
      isModalVisible: false,
    };
  }

  getUpdatedData = () => {
    // Take deep copy of original
    let newData = JSON.parse(JSON.stringify(DATA));

    const transactions = this.state.userPledgeTransactions;
    const totalTransactions = transactions.length;

    let newTotalBacked = newData.totalBackedAmount;
    let newNumBackers = newData.numBackers - totalTransactions;
    let newPledges = newData.pledges;

    for (let i = 0; i < totalTransactions; i++) {
      let transaction = transactions[i];
      newTotalBacked += transaction.pledgeAmount;
      let pledgeData = newPledges.find(
        (pledge) => pledge.id === transaction.pledgeId
      );
      if (pledgeData) pledgeData.numPledgesLeft -= 1;
    }

    newData.numBackers = newNumBackers;
    newData.totalBackedAmount = newTotalBacked;
    newData.pledges = newPledges;

    return newData;
  };

  toggleBookmark = (isBookmarked) => {
    this.setState({
      isBookmarked: isBookmarked ? false : true,
    });
  };

  bookmarkButtonHandler = () => {
    let isBookmarked = this.state.isBookmarked;
    this.toggleBookmark(isBookmarked);
  };

  displayPledgeModal = () => {
    this.setState({
      isModalVisible: true,
    });
  };

  backProductButtonHandler = () => {
    this.displayPledgeModal();
  };

  selectRewardButtonHandler = () => {
    this.displayPledgeModal();
  };

  closePledgeModal = () => {
    this.setState({
      isModalVisible: false,
    });
  };

  closeModalHandler = () => {
    this.closePledgeModal();
  };

  addNewPledge = (pledgeId, pledgeAmount) => {
    let newTransaction = {
      pledgeId,
      pledgeAmount,
      pledgeTime: new Date(),
    };

    let transactionsList = [...this.state.userPledgeTransactions];
    transactionsList.push(newTransaction);

    this.setState({
      userPledgeTransactions: transactionsList,
    });
  };

  submitPledgeHandler = (pledgeId, pledgeAmount) => {
    this.closePledgeModal();
    this.addNewPledge(pledgeId, pledgeAmount);
  };

  componentDidUpdate() {
    // debugger;
  }

  render() {
    const {
      title,
      tagline,
      description,
      currency,
      totalBackedAmount,
      targetAmount,
      numBackers,
      numDaysLeft,
      pledges,
    } = this.getUpdatedData();
    const infoCardData = { title, tagline };

    const metricsCardData = {
      currency,
      totalBackedAmount,
      targetAmount,
      numBackers,
      numDaysLeft,
    };

    const detailsCardData = {
      description,
      pledges,
      currency,
    };

    const pledgeModalData = {
      title,
      pledges,
      currency,
    };
    return (
      <div className={styles.container}>
        <ProjectInfoCard
          data={infoCardData}
          isBookmarked={this.state.isBookmarked}
          onBookmarButtonClick={this.bookmarkButtonHandler}
          onBackProductButtonClick={this.backProductButtonHandler}
        />
        <ProjectMetricsCard data={metricsCardData} />
        <ProjectDetailsCard
          data={detailsCardData}
          onSelectRewardButtonClick={this.selectRewardButtonHandler}
        />
        <PledgeModal
          data={pledgeModalData}
          visible={this.state.isModalVisible}
          closeModalHandler={this.closeModalHandler}
          submitPledgeHandler={this.submitPledgeHandler}
        />
      </div>
    );
  }
}
