import { Button } from "antd";
import React, { Component } from "react";
import projectLogoImage from "../../../images/logo-mastercraft.svg";
import styles from "./ProjectInfoCard.module.css";

export default class ProjectInfoCard extends Component {
  onBookmarButtonClick = () => {
    this.props.onBookmarButtonClick();
  };

  onBackProductButtonClick = () => {
    this.props.onBackProductButtonClick();
  };

  render() {
    return (
      <div className={styles.container}>
        <img
          alt="project-logo"
          src={projectLogoImage}
          className={styles.logo}
        />
        <h2 className={styles.fw_700}>{this.props.data.title}</h2>
        <p className={styles.color_darkgray}>{this.props.data.tagline}</p>
        <div className={styles.button_section}>
          <button
            onClick={this.onBackProductButtonClick}
            className={`${styles.btn} ${styles.button}`}
          >
            Back this project
          </button>
          <button
            onClick={this.onBookmarButtonClick}
            className={`${styles.btn} ${styles.bookmark}`}
          >
            Bookmark
          </button>
        </div>
      </div>
    );
  }
}
