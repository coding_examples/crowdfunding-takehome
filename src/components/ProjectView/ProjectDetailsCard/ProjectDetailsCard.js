import React, { Component } from "react";
import PledgeCard from "../PledgeCard";

export default class ProjectDetailsCard extends Component {
  render() {
    const { pledges, description, currency } = this.props.data;
    return (
      <div>
        <div>
          <h3>About this project</h3>
          <div>{description}</div>
          {pledges.map((pledge) => (
            <PledgeCard
              data={pledge}
              currency={currency}
              onSelectRewardButtonClick={this.props.onSelectRewardButtonClick}
            />
          ))}
        </div>
      </div>
    );
  }
}
